import {Component, OnInit} from '@angular/core';
import {Patient} from '../model/patient.model';
import {PatientService} from '../service/patient.service';
import {Caregiver} from '../model/caregiver.model';
import {CaregiverService} from '../service/caregiver.service';
import {Gender} from '../model/gender.enum';

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {
  patient: Patient;
  caregivers: [Caregiver];
  selectedCaregiver: number;
  constructor(private patientService: PatientService, private caregiverService: CaregiverService) {
    this.patient = new Patient();
    caregiverService.getAllCaregivers().subscribe( data => {
      console.log(data);
      this.caregivers = data;
    });
  }

  ngOnInit() {
    this.patient = JSON.parse(sessionStorage.getItem('selectedPatient'));
  }

  public updatePatient() {
    if (this.selectedCaregiver) {
      this.patientService.updatePatient(this.patient, this.caregivers[this.selectedCaregiver].id).subscribe();
    } else {
      this.patientService.updatePatient(this.patient, -1).subscribe();
    }
  }
}
