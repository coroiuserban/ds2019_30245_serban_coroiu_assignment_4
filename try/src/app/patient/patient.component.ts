import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoginService} from '../service/login.service';
import {Caregiver} from '../model/caregiver.model';
import {MedicationPlan} from '../model/medication-plan.model';
import {PatientService} from '../service/patient.service';
import {Patient} from '../model/patient.model';
import {Doctor} from '../model/doctor.model';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit, AfterViewInit {
  patient: Patient;
  doctor: Doctor;
  caregiver: Caregiver;
  constructor(private patientService: PatientService) {
    this.patient = new Patient();
  }

  ngOnInit() {
    this.doctor = new Doctor();
    this.caregiver = new Caregiver();
    this.patient.medicationPlan = [];
  }

  ngAfterViewInit(): void {
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.patientService.getPatientWithId(loggedUser.id).subscribe(data => {
      this.doctor = data.doctor as Doctor;
      console.log(data.medicationPlan)
      this.patient = data;
    });

    this.patientService.getCaregiverForPatient(loggedUser.id).subscribe( data => {
      this.caregiver = data;
    });
  }
  public logout() {
    sessionStorage.setItem('loggedUser', JSON.stringify(null));
    window.history.back();
  }

}
