import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../model/caregiver.model';
import {CaregiverService} from '../service/caregiver.service';
import {Patient} from '../model/patient.model';

@Component({
  selector: 'app-add-caregiver',
  templateUrl: './add-caregiver.component.html',
  styleUrls: ['./add-caregiver.component.css']
})
export class AddCaregiverComponent implements OnInit {
  caregiver: Caregiver;
  constructor(private caregiverService: CaregiverService) { }

  ngOnInit() {
    this.caregiver = new Caregiver();
  }

  public createCaregiver() {
    this.caregiver.id = 1;
    // console.log(this.caregiver);
    this.caregiverService.createCaregiver(this.caregiver).subscribe(data => {
      window.history.back();
  });
  }

}
