import { Component, OnInit } from '@angular/core';
import {PatientService} from '../service/patient.service';
import {Patient} from '../model/patient.model';
import {Router} from '@angular/router';
import {DoctorService} from '../service/doctor.service';

@Component({
  selector: 'app-list-patients',
  templateUrl: './list-patients.component.html',
  styleUrls: ['./list-patients.component.css']
})
export class ListPatientsComponent implements OnInit {
  patients: [Patient];
  selectedPatient: number;
  constructor(private patientService: PatientService, private doctorService: DoctorService, private router: Router) { }

  ngOnInit() {
    this.doctorService.getAllPatients().subscribe(data => {
      console.log(data);
      this.patients = data;
    });
  }

  public createPatient() {
    this.router.navigateByUrl('doctor/add-patient');
  }
  public deletePatient() {
    if (this.selectedPatient) {
      this.patientService.deletePatient(this.patients[this.selectedPatient].id).subscribe(data => {
        location.reload();
      });
    }
  }

  public createMedicationPlan() {
    if (this.selectedPatient) {
      sessionStorage.setItem('selectedPatient', JSON.stringify(this.patients[this.selectedPatient]));
      this.router.navigateByUrl('doctor/add-medication-plan');
    }
  }

  public updatePatient() {
    if (this.selectedPatient) {
      sessionStorage.setItem('selectedPatient', JSON.stringify(this.patients[this.selectedPatient]));
      this.router.navigateByUrl('doctor/update-patient');
    }
  }

  public listActivities() {
    if (this.selectedPatient) {
      sessionStorage.setItem('selectedPatient', JSON.stringify(this.patients[this.selectedPatient]));
      this.router.navigateByUrl('activities');
    }
  }

  public listMedicationLog() {
    if (this.selectedPatient) {
      sessionStorage.setItem('selectedPatient', JSON.stringify(this.patients[this.selectedPatient]));
      this.router.navigateByUrl('medication-log');
    }
  }
}
