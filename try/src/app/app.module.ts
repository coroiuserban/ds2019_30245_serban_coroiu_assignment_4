import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import { DoctorComponent } from './doctor/doctor.component';
import { CaregiverComponent } from './caregiver/caregiver.component';
import { PatientComponent } from './patient/patient.component';
import { UserComponent } from './user/user.component';
import { ListPatientsComponent } from './list-patients/list-patients.component';
import { AddPatientComponent } from './add-patient/add-patient.component';
import { ListCaregiversComponent } from './list-caregivers/list-caregivers.component';
import { AddCaregiverComponent } from './add-caregiver/add-caregiver.component';
import { AssignCaregiverComponent } from './assign-caregiver/assign-caregiver.component';
import { AddMedicationPlanComponent } from './add-medication-plan/add-medication-plan.component';
import { ListMedicationPlansComponent } from './list-medication-plans/list-medication-plans.component';
import { ListMedicationsComponent } from './list-medications/list-medications.component';
import { AddMedicationComponent } from './add-medication/add-medication.component';
import { UpdatePatientComponent } from './update-patient/update-patient.component';
import { UpdateCaregiverComponent } from './update-caregiver/update-caregiver.component';
import {AuthenticationGuardService} from './service/authentication-guard.service';
import {InterceptorService} from './interceptor.service';
import { AddDoctorComponent } from './add-doctor/add-doctor.component';
import { ActivitiesComponent } from './activities/activities.component';
import { MedicationLogComponent } from './medication-log/medication-log.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DoctorComponent,
    CaregiverComponent,
    PatientComponent,
    UserComponent,
    ListPatientsComponent,
    AddPatientComponent,
    ListCaregiversComponent,
    AddCaregiverComponent,
    AssignCaregiverComponent,
    AddMedicationPlanComponent,
    ListMedicationPlansComponent,
    ListMedicationsComponent,
    AddMedicationComponent,
    UpdatePatientComponent,
    UpdateCaregiverComponent,
    AddDoctorComponent,
    ActivitiesComponent,
    MedicationLogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
        {path: '', component: LoginComponent},
      {path: 'doctor', component: DoctorComponent, canActivate: [AuthenticationGuardService]},
      {path: 'caregiver', component: CaregiverComponent, canActivate: [AuthenticationGuardService]},
      {path: 'patient', component: PatientComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/list-patients', component: ListPatientsComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/add-patient', component: AddPatientComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/list-caregivers', component: ListCaregiversComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/add-caregiver', component: AddCaregiverComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/assign-caregiver', component: AssignCaregiverComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/list-medication-plans', component: ListMedicationPlansComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/add-medication-plan', component: AddMedicationPlanComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/list-medications', component: ListMedicationsComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/add-medication', component: AddMedicationComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/update-patient', component: UpdatePatientComponent, canActivate: [AuthenticationGuardService]},
      {path: 'doctor/update-caregiver', component: UpdateCaregiverComponent, canActivate: [AuthenticationGuardService]},
      {path: 'add-doctor', component: AddDoctorComponent},
      {path: 'activities', component: ActivitiesComponent},
      {path: 'medication-log', component: MedicationLogComponent}
    ])
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ]
})
export class AppModule { }
