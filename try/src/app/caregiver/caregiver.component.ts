import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../model/caregiver.model';
import {CaregiverService} from '../service/caregiver.service';
import {Patient} from '../model/patient.model';
import {LoginService} from '../service/login.service';
import {RecomandationDTO} from '../model/recomandation-dto.model';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {
  caregiver: Caregiver = new Caregiver();
  constructor(private caregiverService: CaregiverService, private loginService: LoginService) {
    this.caregiver.patients = [new Patient()];
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    const stompClient = loginService.connect();
    // tslint:disable-next-line:only-arrow-functions
    stompClient.connect({}, frame => {
      console.log("connected");
      stompClient.subscribe('/notification/' + loggedUser.id, notification => {
        window.alert(notification.body)
      })
    })
  }

  ngOnInit() {
    const loggedUser = JSON.parse(sessionStorage.getItem('loggedUser'));
    this.caregiverService.getCaregiverWithId(loggedUser.id).subscribe( data => {
      this.caregiver = data;
    });
  }

  public logout() {
    sessionStorage.setItem('loggedUser', JSON.stringify(null));
    window.history.back();
  }

  public showRecomandations(forPatientId: number) {
    this.caregiverService.getRecomandationsForPatient(forPatientId).subscribe( data => {
      let msg = [];
      data.forEach(function(value) {
        msg.push(value.message)
      });
      window.alert(msg);
    });
  }
}
