import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user.model';
import {Patient} from '../model/patient.model';
import {Caregiver} from '../model/caregiver.model';
import {Medication} from '../model/medication.model';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private patientUrl: string;
  constructor(private http: HttpClient) {
    this.patientUrl = 'http://localhost:8080/patient';
  }

  public getPatientWithId(id: number) {
    return this.http.post<Patient>(this.patientUrl + '/', id);
  }

  public getAllPatients() {
    return this.http.get<[Patient]>(this.patientUrl);
  }

  public createPatient(patient: Patient) {
    const doctorId: User =  JSON.parse(sessionStorage.getItem('loggedUser'));
    return this.http.post(`${this.patientUrl}/${doctorId.id}/create`, patient);
  }

  public deletePatient(patientId: number) {
    return this.http.delete(`${this.patientUrl}/${patientId}/delete`);
  }

  public assignCaregiver(patientId: number, caregiverId: number) {
    return this.http.post(`${this.patientUrl}/${patientId}/assign`, caregiverId);
  }

  public createMedication(medication: Medication) {
    return this.http.post(`${this.patientUrl}/createMedication`, medication);
  }

  public updatePatient(patient: Patient, caregiverId: number) {
    return this.http.post(`${this.patientUrl}/update/${caregiverId}`, patient);
  }

  public getCaregiverForPatient(patientId: number) {
    return this.http.get<Caregiver>(`${this.patientUrl}/${patientId}/caregiver`);
  }
}
