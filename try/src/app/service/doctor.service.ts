import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Caregiver} from '../model/caregiver.model';
import {Patient} from '../model/patient.model';
import {LoginService} from './login.service';
import {User} from '../model/user.model';
import {MedicationPlan} from '../model/medication-plan.model';
import {Medication} from '../model/medication.model';
import {AuthenticationGuardService} from './authentication-guard.service';
import {Activity} from '../model/activity.model';
import {LocalDate, LocalDateTime} from '@js-joda/core';
import {MedicationStatusLog} from '../model/medication-status-log.model';
import {stringify} from 'querystring';
import {RecomandationDTO} from '../model/recomandation-dto.model';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {

  private doctorUrl: string;
  private ass4Url: string;

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.doctorUrl = 'http://localhost:8080/doctor';
    this.ass4Url = 'http://localhost:8081/assignment4';
  }

  public getAllPatients() {
    const doctorId: User = JSON.parse(sessionStorage.getItem('loggedUser'));
    const headers = new HttpHeaders();
    headers.append('authorization', doctorId.role);
    return this.http.get<[Patient]>(`${this.doctorUrl}/${doctorId.id}/patients`, {headers});
  }

  public getAllMedicationPlans() {
    const doctorId: User = JSON.parse(sessionStorage.getItem('loggedUser'));
    const headers = new HttpHeaders();
    headers.append('authorization', doctorId.role);
    return this.http.get<[MedicationPlan]>(`${this.doctorUrl}/${doctorId.id}/medicationPlans`);
  }

  public getMedications() {
    return this.http.get<[Medication]>(`${this.doctorUrl}/medications`);
  }


  //---------------------for assignment  4------------------------------------
  public getActivities(patientId: number, forDate: LocalDate) {
    return this.http.post<[Activity]>(this.ass4Url +`/activities/${patientId}`, forDate);
  }

  public getMedicationLog(patientId: number, forDate: LocalDate) {
    return this.http.post<[MedicationStatusLog]>(this.ass4Url +`/medicationLog/${patientId}`, forDate);
  }

  public markActivityAsNormal(activityId: number) {
    return this.http.put(this.ass4Url +`/activities/valid`, activityId)
  }

  public createRecomandation(forPatientId: number, activity: number, message: string) {
    let recomandation = new RecomandationDTO(activity, forPatientId, message);
    return this.http.post(this.ass4Url +`/activities/recomandation`, recomandation)
  }
}
