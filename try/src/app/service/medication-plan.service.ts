import { Injectable } from '@angular/core';
import {MedicationPlan} from '../model/medication-plan.model';
import {HttpClient} from '@angular/common/http';
import {LoginService} from './login.service';
import {User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class MedicationPlanService {

  private medicationPlanUrl: string;

  constructor(private http: HttpClient, private loginService: LoginService) {
    this.medicationPlanUrl = 'http://localhost:8080/medicationPlan';
  }

  public createMedicationPlan(medicationPlan: MedicationPlan, patientId: number) {
    const doctorId: User = JSON.parse(sessionStorage.getItem('loggedUser'));
    return this.http.post(`${this.medicationPlanUrl}/${doctorId.id}/${patientId}/create`, medicationPlan);
  }
}
