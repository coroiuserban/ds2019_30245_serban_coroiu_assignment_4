import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Patient} from '../model/patient.model';
import {Caregiver} from '../model/caregiver.model';
import {RecomandationDTO} from '../model/recomandation-dto.model';

@Injectable({
  providedIn: 'root'
})
export class CaregiverService {

  private caregiverUrl: string;
  private ass4Url;
  constructor(private http: HttpClient) {
    this.caregiverUrl = 'http://localhost:8080/caregiver';
    this.ass4Url = 'http://localhost:8081/assignment4';
  }

  public getCaregiverWithId(id: number) {
    return this.http.post<Caregiver>(this.caregiverUrl + '/', id);
  }

  public getAllCaregivers() {
    return this.http.get<[Caregiver]>(this.caregiverUrl);
  }

  public deleteCaregiver(caregiverId: number) {
    return this.http.delete(`${this.caregiverUrl}/${caregiverId}/delete`);
  }

  public createCaregiver(caregiver: Caregiver) {
    return this.http.post(this.caregiverUrl + '/create', caregiver);
  }

  public updateCaregiver(caregiver: Caregiver) {
    return this.http.post(this.caregiverUrl + '/update', caregiver);
  }

  public getPatientsForCaregiver(caregiverId: number) {
    return this.http.get<[Patient]>(`${this.caregiverUrl}/${caregiverId}/patients`);
  }
  //---------------------for assignment  4------------------------------------

  public getRecomandationsForPatient(patientId: number) {
    return this.http.get<[RecomandationDTO]>(this.ass4Url + `/activities/recomandation/${patientId}`);
  }
}
