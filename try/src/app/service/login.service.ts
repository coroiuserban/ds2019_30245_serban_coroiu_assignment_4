import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {USERTYPE} from '../model/usertype.enum';
import {Router} from '@angular/router';
import {User} from '../model/user.model';
import {Doctor} from '../model/doctor.model';
import Stomp from 'stompjs';
import SockJs from 'sockjs-client';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public user: User;
  private loginUrl: string;
  constructor(private http: HttpClient) {
    this.loginUrl = 'http://localhost:8080/login';
  }

  public loginUser(username: string, password: string) {
    const data = new FormData();
    data.append('username', username);
    data.append('password', password);
    return this.http.post<User>(this.loginUrl + '/login', data);
  }

  public createDoctor(doctor: Doctor) {
      return this.http.post(this.loginUrl + '/create', doctor);
  }

  public connect() {
    const socket = new SockJs('http://localhost:8080/socket');
    return Stomp.over(socket);
  }

}
