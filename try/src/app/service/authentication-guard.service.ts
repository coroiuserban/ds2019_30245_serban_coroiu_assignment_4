import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {USERTYPE} from '../model/usertype.enum';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuardService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    const userLogged = JSON.parse(sessionStorage.getItem('loggedUser'));
    const doctorRegex = /doctor\/*[a-zA-Z\/]*/;
    const patientRegex = /patient\/*[a-zA-Z\/]*/;
    const caregiverRegex = /caregiver\/*[a-zA-Z\/]*/;

    console.log(userLogged);

    if (userLogged === null) {
      this.router.navigate(['']);
      return false;
    }

    // console.log(route.parent)
    // console.log(userLogged.role)
    // console.log(userLogged.role === 'Doctor');

    if (userLogged.role === USERTYPE.Doctor && doctorRegex.test(route.routeConfig.path)) {
      return true;
    } else if (userLogged.role === USERTYPE.Patient && patientRegex.test(route.routeConfig.path) ) {
      return true;
    } else if (userLogged.role === USERTYPE.Caregiver && caregiverRegex.test(route.routeConfig.path) ) {
      return true;
    }

    this.router.navigate(['']);
    return false;
  }
}
