import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userUrl: string;
  public userSelected: User;
  constructor(private http: HttpClient) {
    this.userUrl = 'http://localhost:8080/user';
  }

  public getUsers() {
    return this.http.get<[User]>(this.userUrl);
  }

  public deleteUser(userId: number): void {
    this.http.delete(`${this.userUrl}/${userId}/delete`).subscribe();
  }
}
