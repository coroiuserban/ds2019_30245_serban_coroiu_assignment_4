import { Component, OnInit } from '@angular/core';
import {UserService} from '../service/user.service';
import {User} from '../model/user.model';
import {USERTYPE} from '../model/usertype.enum';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private user: User;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user = this.userService.userSelected;
  }

  public updateUser() {
    console.log(this.user);
  }
}

