import { Component, OnInit } from '@angular/core';
import {Doctor} from '../model/doctor.model';
import {LoginService} from '../service/login.service';

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.css']
})
export class AddDoctorComponent implements OnInit {
  doctor: Doctor;
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.doctor = new Doctor();
  }
  public createDoctor() {
    this.loginService.createDoctor(this.doctor).subscribe().add(data => {
      window.history.back();
    })
    ;
  }
}
