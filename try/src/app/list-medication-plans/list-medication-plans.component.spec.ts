import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMedicationPlansComponent } from './list-medication-plans.component';

describe('ListMedicationPlansComponent', () => {
  let component: ListMedicationPlansComponent;
  let fixture: ComponentFixture<ListMedicationPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMedicationPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMedicationPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
