import { Component, OnInit } from '@angular/core';
import {Medication} from '../model/medication.model';
import {MedicationPlan} from '../model/medication-plan.model';
import {DoctorService} from '../service/doctor.service';

@Component({
  selector: 'app-list-medication-plans',
  templateUrl: './list-medication-plans.component.html',
  styleUrls: ['./list-medication-plans.component.css']
})
export class ListMedicationPlansComponent implements OnInit {
  medicationPlans: [MedicationPlan];

  constructor(private doctorService: DoctorService) { }

  ngOnInit() {
    this.doctorService.getAllMedicationPlans().subscribe(data => {
      this.medicationPlans = data;
    });
  }
}
