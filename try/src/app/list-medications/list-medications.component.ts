import { Component, OnInit } from '@angular/core';
import {DoctorService} from '../service/doctor.service';
import {Medication} from '../model/medication.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-list-medications',
  templateUrl: './list-medications.component.html',
  styleUrls: ['./list-medications.component.css']
})
export class ListMedicationsComponent implements OnInit {
  medications: [Medication];
  constructor(private doctorService: DoctorService, private router: Router) { }

  ngOnInit() {
    this.doctorService.getMedications().subscribe(data => {
      this.medications = data;
    });
  }

  public createMedication() {
    this.router.navigateByUrl('doctor/add-medication');
  }

}

