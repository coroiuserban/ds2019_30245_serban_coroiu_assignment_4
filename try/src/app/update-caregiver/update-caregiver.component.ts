import { Component, OnInit } from '@angular/core';
import {Caregiver} from '../model/caregiver.model';
import {CaregiverService} from '../service/caregiver.service';

@Component({
  selector: 'app-update-caregiver',
  templateUrl: './update-caregiver.component.html',
  styleUrls: ['./update-caregiver.component.css']
})
export class UpdateCaregiverComponent implements OnInit {
  caregiver: Caregiver;
  constructor(private caregiverService: CaregiverService) {
   this.caregiver = new Caregiver();
  }

  ngOnInit() {
    this.caregiver = JSON.parse(sessionStorage.getItem('selectedCaregiver'));
    console.log(this.caregiver);
  }

  public updateSelected() {
    this.caregiverService.updateCaregiver(this.caregiver).subscribe();
  }
}
