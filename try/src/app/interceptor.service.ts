import { Injectable } from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from './model/user.model';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // how to updatedRequeste the request Parameters
    const loggedUser: User = JSON.parse(sessionStorage.getItem('loggedUser'));
    if (loggedUser == null) {
      return next.handle(req);
    }
    const updatedRequest = req.clone({
      headers: req.headers.set('authorization', loggedUser.role)
    });
    // logging the updated Parameters to browser's console
    console.log('Before making api call : ', updatedRequest);
    return next.handle(updatedRequest);
  }
}
