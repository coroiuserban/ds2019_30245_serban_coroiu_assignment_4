import { Component, OnInit } from '@angular/core';
import {CaregiverService} from '../service/caregiver.service';
import {Router} from '@angular/router';
import {Caregiver} from '../model/caregiver.model';

@Component({
  selector: 'app-list-caregivers',
  templateUrl: './list-caregivers.component.html',
  styleUrls: ['./list-caregivers.component.css']
})
export class ListCaregiversComponent implements OnInit {
  caregivers: [Caregiver];
  selectedCaregiver: number;
  constructor(private caregiverService: CaregiverService, private router: Router) {
  }

  ngOnInit() {
    this.caregiverService.getAllCaregivers().subscribe(data => {
      this.caregivers = data;
      // console.log(data)
    });
  }
  public createCaregiver() {
    this.router.navigateByUrl('doctor/add-caregiver');
  }
  public deleteCaregiver() {
    if (this.selectedCaregiver) {
      this.caregiverService.deleteCaregiver(this.caregivers[this.selectedCaregiver].id).subscribe(a => {
        location.reload();
      });
    }
  }

  public updateCaregiver() {
    console.log(this.caregivers[this.selectedCaregiver]);
    if (this.selectedCaregiver) {
      console.log(this.caregivers[this.selectedCaregiver]);
      sessionStorage.setItem('selectedCaregiver', JSON.stringify(this.caregivers[this.selectedCaregiver]));
      this.router.navigateByUrl('doctor/update-caregiver');
    }
  }

}
