export class RecomandationDTO {
  id: number;
  activityId: number;
  patientId: number;
  message: string;
  constructor(activityId?: number, patientId?: number, message?: string, id?:number) {
    this.id = id;
    this.patientId = patientId;
    this.activityId = activityId;
    this.message = message;
  }

}
