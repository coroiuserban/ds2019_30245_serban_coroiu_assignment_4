import {User} from './user.model';
import {MedicationPlan} from './medication-plan.model';
import {USERTYPE} from './usertype.enum';
import {Gender} from './gender.enum';
import {Caregiver} from './caregiver.model';
import {Doctor} from './doctor.model';

export class Patient extends User {
  medicationPlan: MedicationPlan[];
  medicalRecord: string;
  doctor: Doctor;
  // tslint:disable-next-line:max-line-length
  constructor(id?: number, username?: string, name?: string, password?: string, address?: string, role?: USERTYPE, gender?: Gender, birthDate?: Date, medicalRecord?: string, medicationPlan?: MedicationPlan[], doctor?: Doctor) {
    super(id, username, name, password, address, role, Gender.Female, birthDate);
    this.medicationPlan = medicationPlan;
    this.medicalRecord = medicalRecord;
    this.doctor = doctor;
    this.role = USERTYPE.Patient;
  }
}
