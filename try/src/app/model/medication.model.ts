export class Medication {
  id: number;
  name: string;
  sideEffects: string;
  dosage: number;

  constructor(id?: number, name?: string, sideEffects?: string, dosage?: number) {
    this.id = id;
    this.name = name;
    this.sideEffects = sideEffects;
    this.dosage = dosage;
  }
}
