import {USERTYPE} from './usertype.enum';
import {Gender} from './gender.enum';

export class User {
  id: number;
  username: string;
  name: string;
  password: string;
  address: string;
  role: string;
  gender: string;
  birthDate: Date;


  // tslint:disable-next-line:max-line-length
  constructor(id?: number, username?: string, name?: string, password?: string, address?: string, role?: USERTYPE, gender?: Gender, birthDate?: Date) {
    this.id = id;
    this.username = username;
    this.name = name;
    this.password = password;
    this.address = address;
    this.role = role;
    this.gender = gender;
    this.birthDate = birthDate;
  }

  toString(): string {
    return this.name + ' ' + this.role;
  }
}
