import {Medication} from './medication.model';
import {Patient} from './patient.model';

export class MedicationPlan {
  id: number;
  medicationPlan: Medication[];
  intakeIntervals: string[];
  period: number;
  patient: Patient;
  startTime: Date;

  constructor(id?: number, medicationPlan?: Medication[], period?: number, patient?: Patient, intakeIntervals?: string[], startTime?: Date) {
    this.id = id;
    this.medicationPlan = medicationPlan;
    this.intakeIntervals = intakeIntervals;
    this.period = period;
    this.patient = patient;
    this.startTime = startTime;
  }
}
