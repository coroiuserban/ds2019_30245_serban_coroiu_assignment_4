import {User} from './user.model';
import {Patient} from './patient.model';
import {USERTYPE} from './usertype.enum';
import {Gender} from './gender.enum';

export class Caregiver extends User {
  patients: [Patient];
  // tslint:disable-next-line:max-line-length
  constructor(id?: number, username?: string, name?: string, password?: string, address?: string, role?: string, gender?: Gender, birthDate?: Date, patients?: [Patient]) {
    super(id, username, name, password, address, USERTYPE.Caregiver, gender, birthDate);
    this.role = USERTYPE.Caregiver;
    this.patients = patients;
  }
}
