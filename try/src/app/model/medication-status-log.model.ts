import {LocalDateTime} from '@js-joda/core';

export class MedicationStatusLog {
  id: number;
  message: string;
  patientId: number;
  date: LocalDateTime;

  constructor(id?: number, message?: string, patientId?: number, date?: LocalDateTime) {
    this.id = id;
    this.message = message;
    this.patientId = patientId;
    this.date = date;
  }
}
