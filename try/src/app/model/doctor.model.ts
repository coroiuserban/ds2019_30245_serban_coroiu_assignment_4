import {User} from './user.model';
import {MedicationPlan} from './medication-plan.model';
import {USERTYPE} from './usertype.enum';
import {Gender} from './gender.enum';
import {Patient} from './patient.model';

export class Doctor extends User {
  medicationPlan: MedicationPlan[];
  patient: [Patient];
  // tslint:disable-next-line:max-line-length
  constructor(id?: number, username?: string, name?: string, password?: string, address?: string, role?: USERTYPE, gender?: Gender, birthDate?: Date, medicalRecord?: string, medicationPlan?: MedicationPlan[], patient?: [Patient]) {
    super(id, username, name, password, address, role, Gender.Female, birthDate);
    this.medicationPlan = medicationPlan;
    this.patient = patient;
    this.role = USERTYPE.Doctor;
  }

}
