import {LocalDateTime} from '@js-joda/core';

export class Activity {
  activityId: number;
  duration: number;
  startTime: LocalDateTime;
  activity: string;
  valid: boolean;
  constructor(activityId?: number, duration?: number, startTime?: LocalDateTime, activity?: string, valid?: boolean) {
    this.activity = activity;
    this.duration = duration;
    this.startTime = startTime;
    this.activityId = activityId;
    this.valid = valid;
  }
}
