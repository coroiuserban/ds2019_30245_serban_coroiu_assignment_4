import { Component, OnInit } from '@angular/core';
import * as CanvasJS from '../canvasjs.min';
import {CaregiverService} from '../service/caregiver.service';
import {DoctorService} from '../service/doctor.service';
import {element} from 'protractor';
import {LocalDate, LocalDateTime} from '@js-joda/core';
import {Activity} from '../model/activity.model';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {
  private chart: CanvasJS.Chart;
  private data = [];
  private dateSelected: string;
  private activities = [];
  private message = [];
  constructor(private doctorService: DoctorService) { }

  ngOnInit() {
    this.chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title:{
        text: JSON.parse(sessionStorage.getItem('selectedPatient')).name + '\' activity'
      },
      data: [{
          type: "pie",
          showInLegend: true,
          toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
          indexLabel: "{name} - #percent%",
          dataPoints: this.data
        }]
    });
    // this.chart.render();
  }

  public getActivities() {
    let patientId = JSON.parse(sessionStorage.getItem('selectedPatient'));
    this.doctorService.getActivities(patientId.id, LocalDate.parse(this.dateSelected)).subscribe(data => {
      this.activities = data;
      this.createChart();
    })

  }

  public createChart() {
    //if(this.activities.length < 1) { return }

    var activityMap = [];
    this.activities.forEach(function(value) {
      if (activityMap[value.activity]) {
        activityMap[value.activity] += value.duration
      } else {
        activityMap[value.activity] = value.duration
      }
    });

    // console.log(activityMap);
    var dataPoints = [];
    for (var key in activityMap) {
      var value = activityMap[key];
        dataPoints.push({
        y: value, name: key
      });
    }
    //console.log(this.data);
    // console.log(dataPoints);
    this.chart = new CanvasJS.Chart("chartContainer", {
      theme: "light2",
      animationEnabled: true,
      exportEnabled: true,
      title:{
        text: JSON.parse(sessionStorage.getItem('selectedPatient')).name + '\' activity'
      },
      data: [{
        type: "pie",
        showInLegend: true,
        toolTipContent: "<b>{name}</b>: ${y} (#percent%)",
        indexLabel: "{name} - #percent%",
        dataPoints: dataPoints
      }]
    });
    this.chart.render();
  }
  public findActivities() {
    this.getActivities();
    //console.log(this.activities);
    // this.createChart();
  }
  public markNormal(index: number) {
    this.doctorService.markActivityAsNormal(this.activities[index].activityId).subscribe();
  }

  public createRecomandation(index: number, msg: string) {
    let patient = JSON.parse(sessionStorage.getItem('selectedPatient'));
    this.doctorService.createRecomandation(patient.id, index, msg).subscribe();
  }
}


// var dt = LocalDateTime.parse(this.activities[0].startTime);
// console.log(dt.year());
