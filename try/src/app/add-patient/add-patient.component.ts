import { Component, OnInit } from '@angular/core';
import {Patient} from '../model/patient.model';
import {PatientService} from '../service/patient.service';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {
  patient: Patient;
  constructor(private patientService: PatientService) { }

  ngOnInit() {
    this.patient = new Patient();
  }

  public createPatient() {
    this.patient.id = 1;
    console.log(this.patient);
    this.patientService.createPatient(this.patient).subscribe(data => {
      window.history.back();
    });
  }
}
