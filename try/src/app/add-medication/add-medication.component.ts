import { Component, OnInit } from '@angular/core';
import {Medication} from '../model/medication.model';
import {element} from 'protractor';
import {DoctorService} from '../service/doctor.service';
import {PatientService} from '../service/patient.service';

@Component({
  selector: 'app-add-medication',
  templateUrl: './add-medication.component.html',
  styleUrls: ['./add-medication.component.css']
})
export class AddMedicationComponent implements OnInit {
  medication: Medication;
  constructor(private patientService: PatientService) {
    this.medication = new Medication();
  }

  ngOnInit() {
  }

  public addSideEffect() {
      // console.log(this.medication);
      if(this.medication.sideEffects && this.medication.dosage && this.medication.name) {
        this.patientService.createMedication(this.medication).subscribe(data => {
          window.history.back();
        })
      }
  }
}
