import { Component, OnInit } from '@angular/core';
import {Medication} from '../model/medication.model';
import {DoctorService} from '../service/doctor.service';
import {Patient} from '../model/patient.model';
import {MedicationPlanService} from '../service/medication-plan.service';
import {MedicationPlan} from '../model/medication-plan.model';
import {element} from 'protractor';

@Component({
  selector: 'app-add-medication-plan',
  templateUrl: './add-medication-plan.component.html',
  styleUrls: ['./add-medication-plan.component.css']
})
export class AddMedicationPlanComponent implements OnInit {
  medications: [Medication];
  selectedMedications: Medication[];
  selectedMedication: number;
  forPatient: Patient;
  medicationPlanPeriod: number;
  medicationPlanStart: Date;
  intakeIntervals: boolean[] =[false, false, false];
  intakeIntervalsSelected: string[] =[];
  constructor(private doctorService: DoctorService, private medicationPlanService: MedicationPlanService) {
    this.doctorService.getMedications().subscribe(data => {
      console.log(data);
      this.medications = data;
    });
  }

  ngOnInit() {
    this.forPatient = JSON.parse(sessionStorage.getItem('selectedPatient'));
    this.selectedMedications = [];
  }

  public addMed() {
    if (this.selectedMedication && (this.intakeIntervals[0] || this.intakeIntervals[1] || this.intakeIntervals[2])) {
      this.intakeIntervalsSelected.push(`${Number(this.intakeIntervals[0])}-${Number(this.intakeIntervals[1])}-${Number(this.intakeIntervals[2])}`);
      this.intakeIntervals = [false, false, false];
      this.selectedMedications.push(this.medications[this.selectedMedication]);
    }
  }

  public createMedicationPlan() {
    if (this.selectedMedications.length > 0 && this.medicationPlanPeriod) {
      const medPlan = new MedicationPlan();
      medPlan.patient = this.forPatient;
      medPlan.period = this.medicationPlanPeriod;
      medPlan.medicationPlan = this.selectedMedications;
      medPlan.startTime = this.medicationPlanStart;
      medPlan.intakeIntervals = this.intakeIntervalsSelected;
      this.medicationPlanService.createMedicationPlan(medPlan, this.forPatient.id).subscribe( data => {
        window.history.back();
      });
    }
  }
}
