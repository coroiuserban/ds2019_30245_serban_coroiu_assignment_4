import { Component, OnInit } from '@angular/core';
import {DoctorService} from '../service/doctor.service';
import {User} from '../model/user.model';
import {LocalDate, LocalDateTime} from '@js-joda/core';
import {MedicationStatusLog} from '../model/medication-status-log.model';

@Component({
  selector: 'app-medication-log',
  templateUrl: './medication-log.component.html',
  styleUrls: ['./medication-log.component.css']
})
export class MedicationLogComponent implements OnInit {
  private dateSelected: string;
  private medicationLog: [MedicationStatusLog];
  constructor(private doctorService: DoctorService) { }

  ngOnInit() {
    // @ts-ignore
    this.medicationLog = [];
  }

  public loadMedicationLog() {
    const patient = JSON.parse(sessionStorage.getItem('selectedPatient'));
    let date = LocalDate.parse(this.dateSelected);
    this.doctorService.getMedicationLog(patient.id, date).subscribe(data => {
      console.log(data[0]);
      this.medicationLog = data;
    })
  }

}
