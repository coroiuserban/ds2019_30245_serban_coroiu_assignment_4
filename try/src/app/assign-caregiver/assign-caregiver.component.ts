import { Component, OnInit } from '@angular/core';
import {Patient} from '../model/patient.model';
import {PatientService} from '../service/patient.service';
import {Router} from '@angular/router';
import {CaregiverService} from '../service/caregiver.service';
import {Caregiver} from '../model/caregiver.model';
import {DoctorService} from '../service/doctor.service';

@Component({
  selector: 'app-assign-caregiver',
  templateUrl: './assign-caregiver.component.html',
  styleUrls: ['./assign-caregiver.component.css']
})
export class AssignCaregiverComponent implements OnInit {
  patients: [Patient];
  selectedPatient: number;
  caregivers: [Caregiver];
  selectedCaregiver: number;
  constructor(private patientService: PatientService, private router: Router, private caregiverService: CaregiverService, private doctorService: DoctorService) { }

  ngOnInit() {
    this.doctorService.getAllPatients().subscribe(data => {
      this.patients = data;
    });

    this.caregiverService.getAllCaregivers().subscribe(data => {
      this.caregivers = data;
    });
  }

  public assign() {
    if (this.selectedPatient && this.selectedCaregiver) {
      this.patientService.assignCaregiver(this.patients[this.selectedPatient].id, this.caregivers[this.selectedCaregiver].id).subscribe(data=> {
        location.reload();
      });
    }
  }

}
