import {AfterViewInit, Component, OnInit} from '@angular/core';
import {LoginService} from '../service/login.service';
import {PatientService} from '../service/patient.service';
import {Patient} from '../model/patient.model';
import {UserService} from '../service/user.service';
import {User} from '../model/user.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.css']
})
export class DoctorComponent implements OnInit, AfterViewInit {
  users: [User];
  patients: [Patient];
  selectedUser: number;
  // tslint:disable-next-line:max-line-length
  loggedDoctor: User;
  constructor(private loginService: LoginService, private patientService: PatientService, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.loggedDoctor = JSON.parse(sessionStorage.getItem('loggedUser'));
  }

  ngAfterViewInit(): void {
    this.userService.getUsers().subscribe(data => {
      this.users = data;
    });
  }

  public deleteSelectedUser() {
    console.log(this.selectedUser);
    this.userService.deleteUser(this.selectedUser);
  }

  public patientsPage() {
    this.router.navigateByUrl('doctor/list-patients');
  }

  public caregiversPage() {
    this.router.navigateByUrl('doctor/list-caregivers');
  }

  public assignCaregiverPage() {
    this.router.navigateByUrl('doctor/assign-caregiver');
  }

  public medicationPlanPage() {
    this.router.navigateByUrl('doctor/list-medication-plans');
  }

  public medicationPage() {
    this.router.navigateByUrl('doctor/list-medications');
  }

  public logout() {
    sessionStorage.setItem('loggedUser', JSON.stringify(null));
    window.history.back();
  }
}
