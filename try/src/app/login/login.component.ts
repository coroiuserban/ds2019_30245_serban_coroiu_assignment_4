import {Component, OnInit} from '@angular/core';
import {LoginService} from '../service/login.service';
import {USERTYPE} from '../model/usertype.enum';
import {Router} from '@angular/router';
import {User} from '../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public username: string;
  public password: string;
  // public user: User;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  loginButtonPressed() {
    console.log(this.username);
    console.log(this.password);
    this.loginService.loginUser(this.username, this.password).subscribe(data => {
      this.loginService.user = data;
      sessionStorage.setItem('loggedUser', JSON.stringify(data));
      switch (data.role) {
        case USERTYPE.Doctor: {
          this.router.navigateByUrl('doctor');
          break;
        }
        case USERTYPE.Caregiver: {
          this.router.navigateByUrl('caregiver');
          break;
        }
        case USERTYPE.Patient: {
          this.router.navigateByUrl('patient');
          break;
        }
      }
    });
  }

  public addDoctor() {
    this.router.navigateByUrl('add-doctor');
  }

}
