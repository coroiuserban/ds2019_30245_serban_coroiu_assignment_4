package com.sd.assignment3_serverside.config;

import SD.assignment1.service.ClientAssignment3Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@Configuration
public class ConfigureBeans {

    @Bean
    public RmiProxyFactoryBean getClientService() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/ClientAssignment3Service");
        rmiProxyFactory.setServiceInterface(ClientAssignment3Service.class);
        return rmiProxyFactory;
    }

}
