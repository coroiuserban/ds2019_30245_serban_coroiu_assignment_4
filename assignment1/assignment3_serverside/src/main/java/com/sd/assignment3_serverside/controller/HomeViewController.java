package com.sd.assignment3_serverside.controller;

import SD.assignment1.model.Medication;
import SD.assignment1.model.MedicationPlan;
import SD.assignment1.service.ClientAssignment3Service;
import com.sd.assignment3_serverside.model.MedicationStatus;
import com.sd.assignment3_serverside.model.PartOfDay;
import com.sd.assignment3_serverside.model.TableModelDTO;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Font;
import javafx.stage.Popup;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
@EnableScheduling
public class HomeViewController implements Initializable {

    @FXML Label dateLabel;
    @FXML Label titleLabel;
    @FXML Label errorLabel;
    @FXML TableView<TableModelDTO> tableView;
    @FXML TableColumn<TableModelDTO, String> medPlanIdTableColumn;
    @FXML TableColumn<TableModelDTO, String> nameTableColumn;
    @FXML TableColumn<TableModelDTO, String> sideEffectTableColumn;
    @FXML TableColumn<TableModelDTO, String> dosageTableColumn;
    //waiting, expired, taken
    @FXML TableColumn<TableModelDTO, String> statusTableColumn;
    @FXML TableColumn<TableModelDTO, String> partOfDayTableColumn;


    @Autowired
    private ClientAssignment3Service clientService;
    private List<TableModelDTO> medications;
    private long loggedUser = -1;
    //will be used to verify when user want to take a pill
    private PartOfDay partOfDay;

    public HomeViewController() { }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initClock();
//        getMedications();
        setupTable();
        titleLabel.setText("Planul medical pentru prima parte a zilei( pana la ora 12:00)\nPlanul medical pentru a doua parte a zilei( 12:00 - 18:00 )\nPlanul medical pentru ultima parte a zilei( 18:00 - 22:00 )");
        errorLabel.setVisible(false);
        errorLabel.setFont(Font.font(24));
    }

    private void initClock() {
        Timeline clock = new Timeline(new KeyFrame(Duration.ZERO, e -> {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            dateLabel.setText(LocalDateTime.now().format(formatter));
        }), new KeyFrame(Duration.seconds(1)));
        clock.setCycleCount(Animation.INDEFINITE);
        clock.play();
    }

    //sec min hour ..
    @Scheduled(cron = "0 48 21 * * ?")
    private void getMedications() {
        if (loggedUser != -1) {
            List<MedicationPlan> medicationPlans = clientService.getMedications(loggedUser);
            parseMedicationPlans(medicationPlans);
        }
    }

    public long getLoggedUser() {
        return loggedUser;
    }

    public void setLoggedUser(long loggedUser) {
        this.loggedUser = loggedUser;
    }

    private void parseMedicationPlans(List<MedicationPlan> medicationPlans) {
        medications = new ArrayList<>();
        for (MedicationPlan currentMedicationPlan : medicationPlans) {
            for(int i=0;i<currentMedicationPlan.getMedicationPlan().size();i++){
                Medication currentMedication = currentMedicationPlan.getMedicationPlan().get(i);
                String[] intervals = currentMedicationPlan.getIntakeIntervals().get(i).split("-");
                if( intervals[0].equals("1") ) {
                    medications.add(new TableModelDTO(currentMedicationPlan.getId(), currentMedication.getName(),currentMedication.getSideEffects(), currentMedication.getDosage(), MedicationStatus.WAITING, PartOfDay.DIMINEATA));
                }
                if( intervals[1].equals("1") ) {
                    medications.add(new TableModelDTO(currentMedicationPlan.getId(), currentMedication.getName(),currentMedication.getSideEffects(), currentMedication.getDosage(), MedicationStatus.WAITING, PartOfDay.PRANZ));
                }
                if( intervals[2].equals("1") ) {
                    medications.add(new TableModelDTO(currentMedicationPlan.getId(), currentMedication.getName(),currentMedication.getSideEffects(), currentMedication.getDosage(), MedicationStatus.WAITING, PartOfDay.SEARA));
                }
            }
        }
        if(LocalTime.now().isBefore(LocalTime.of(12, 0)) && LocalTime.now().isAfter(LocalTime.of(6,0))) {
            System.out.println("Dimi");
            partOfDay = PartOfDay.DIMINEATA;
        } else if(LocalTime.now().isBefore(LocalTime.of(18, 0))) {
            System.out.println("Pranz");
            partOfDay = PartOfDay.PRANZ;
        } else {
            System.out.println("Seara");
            partOfDay = PartOfDay.SEARA;
        }
        showMedications();
    }

    private void setupTable() {
        medPlanIdTableColumn.setCellValueFactory(new PropertyValueFactory<>("medicationPlanId"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("medicationName"));
        sideEffectTableColumn.setCellValueFactory(new PropertyValueFactory<>("medicationSideEffects"));
        dosageTableColumn.setCellValueFactory(new PropertyValueFactory<>("dosage"));
        statusTableColumn.setCellValueFactory(new PropertyValueFactory<>("status"));
        partOfDayTableColumn.setCellValueFactory(new PropertyValueFactory<>("partOfDay"));

        tableView.getSortOrder().add(partOfDayTableColumn);
    }

    private void setTableItems(List<TableModelDTO> items) {
        Collections.sort(items);
        tableView.getItems().clear();
        tableView.getItems().setAll(items);
    }

    public void showMedications() {
        setTableItems(medications);
    }

    public void takeSelectedMedication() {
        TableModelDTO selected = tableView.getSelectionModel().getSelectedItem();
        if(!partOfDay.equals(selected.getPartOfDay())){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Operatie nepermisa");
            alert.setContentText("Nu poti lua pastilele de: " + selected.getPartOfDay() + ".\nAcum este: " + partOfDay +"!");
            alert.showAndWait();
            return;
        }
        if(!selected.getStatus().equals(MedicationStatus.WAITING)){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Operatie nepermisa");
            alert.setContentText("Nu poti lua aceasta pastila!");
            alert.showAndWait();
            return;
        }
        selected.setStatus(MedicationStatus.TAKED);
        tableView.refresh();
        clientService.medicationLog("TAKED:Medicamentul: " + selected.getMedicationName() + "/ " + selected.getDosage() + " a fost luat in/la " + selected.getPartOfDay() + ".", loggedUser);
    }

    @Scheduled(cron = "0 44 09 * * ?")
    private void checkMorningMedicationStatus() {
        List<TableModelDTO> morningMedications = tableView.getItems().stream().filter(element -> element.getPartOfDay().equals(PartOfDay.DIMINEATA) && element.getStatus().equals(MedicationStatus.WAITING)).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = PartOfDay.PRANZ;
    }

    @Scheduled(cron = "0 0 18 * * ?")
    private void checkNoonMedicationStatus() {
        List<TableModelDTO> morningMedications = tableView.getItems().stream().filter(element -> element.getPartOfDay().equals(PartOfDay.PRANZ) && element.getStatus().equals(MedicationStatus.WAITING)).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = PartOfDay.SEARA;
    }


    @Scheduled(cron = "0 55 23 * * ?")
    private void checkEveningMedicationStatus() {
        List<TableModelDTO> morningMedications = tableView.getItems().stream().filter(element -> element.getPartOfDay().equals(PartOfDay.SEARA) && element.getStatus().equals(MedicationStatus.WAITING)).collect(Collectors.toList());
        updateMedications(morningMedications);
        partOfDay = PartOfDay.DIMINEATA;
    }

    private void updateMedications(List<TableModelDTO> medications) {
        medications.forEach( element -> element.setStatus(MedicationStatus.EXPIRED));
        tableView.refresh();
        medications.forEach( element -> clientService.medicationLog("EXPIRED:Medicamentul: " + element.getMedicationName() + "/ " + element.getDosage() + " nu a fost luat in/la " + element.getPartOfDay() + ".", loggedUser));
    }

}
