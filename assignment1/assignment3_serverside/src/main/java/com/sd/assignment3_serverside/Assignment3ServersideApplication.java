package com.sd.assignment3_serverside;

import SD.assignment1.service.LoginService;
import com.sd.assignment3_serverside.config.ConfigureBeans;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;

@SpringBootApplication
@Import(ConfigureBeans.class)
public class Assignment3ServersideApplication extends Application {

    private Parent parent;

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(parent));
        primaryStage.show();
    }

    @Override
    public void init() throws IOException {
        ApplicationContext springContext = SpringApplication.run(Assignment3ServersideApplication.class);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/LoginView.fxml"));
        loader.setControllerFactory(springContext::getBean);
        parent = loader.load();
    }
}
