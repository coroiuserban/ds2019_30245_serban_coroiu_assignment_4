package com.sd.assignment3_serverside.controller;

import SD.assignment1.service.ClientAssignment3Service;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

@Controller
public class LoginViewController implements Initializable {

    public TextField username;
    public TextField password;

    @Autowired
    private ClientAssignment3Service clientService;
    @Autowired
    private ApplicationContext context;
    @Autowired
    private HomeViewController homeViewController;


    public LoginViewController() {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void loginButtonPressed() {
        System.out.println(username.getText() +" "+ password.getText());
        long loginId = clientService.canLogin(username.getText(), password.getText());
        try {
            if(loginId != -1) {
                Stage stage = (Stage) username.getScene().getWindow();
                FXMLLoader loader = new FXMLLoader(LoginViewController.class.getResource("/HomeView.fxml"));
                homeViewController.setLoggedUser(loginId);
                loader.setController(homeViewController);
                Parent parent = loader.load();
                Scene scene = new Scene(parent);
                stage.setScene(scene);
                stage.show();
            } else {
                throw new Exception("invalid credentials");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
