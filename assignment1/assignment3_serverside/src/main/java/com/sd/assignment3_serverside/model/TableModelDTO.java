package com.sd.assignment3_serverside.model;

public class TableModelDTO implements Comparable<TableModelDTO>{

    private Long medicationPlanId;
    private String medicationName;
    private String medicationSideEffects;
    private int dosage;
    private MedicationStatus status;
    private PartOfDay partOfDay;

    public TableModelDTO(Long medicationPlanId, String medicationName, String medicationSideEffects, int dosage, MedicationStatus status, PartOfDay partOfDay) {
        this.medicationPlanId = medicationPlanId;
        this.medicationName = medicationName;
        this.medicationSideEffects = medicationSideEffects;
        this.dosage = dosage;
        this.status = status;
        this.partOfDay = partOfDay;
    }

    public Long getMedicationPlanId() {
        return medicationPlanId;
    }

    public void setMedicationPlanId(Long medicationPlanId) {
        this.medicationPlanId = medicationPlanId;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getMedicationSideEffects() {
        return medicationSideEffects;
    }

    public void setMedicationSideEffects(String medicationSideEffects) {
        this.medicationSideEffects = medicationSideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public MedicationStatus getStatus() {
        return status;
    }

    public void setStatus(MedicationStatus status) {
        this.status = status;
    }

    public PartOfDay getPartOfDay() {
        return partOfDay;
    }

    public void setPartOfDay(PartOfDay partOfDay) {
        this.partOfDay = partOfDay;
    }

    @Override
    public String toString() {
        return "TableModelDTO{" +
                "medicationPlanId=" + medicationPlanId +
                ", medicationName='" + medicationName + '\'' +
                ", medicationSideEffects='" + medicationSideEffects + '\'' +
                ", dosage=" + dosage +
                ", status=" + status +
                ", partOfDay=" + partOfDay +
                '}';
    }

    @Override
    public int compareTo(TableModelDTO o) {
        return getPartOfDay().compareTo(o.partOfDay);
    }
}
