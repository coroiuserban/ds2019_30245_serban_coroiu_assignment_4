package SD.assignment1.model;

public class PatientActivity {

    private Long activityId;
    private Long duration;
    private String startTime;
    private String activity;
    private boolean valid;

    public PatientActivity(Long activityId, Long duration, String activity, String startTime, boolean valid) {
        this.activityId = activityId;
        this.duration = duration;
        this.activity = activity;
        this.startTime = startTime;
        this.valid = valid;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Override
    public String toString() {
        return "PatientActivity{" +
                "activityIId=" + activityId +
                ", duration=" + duration +
                ", activity='" + activity + '\'' +
                '}';
    }
}
