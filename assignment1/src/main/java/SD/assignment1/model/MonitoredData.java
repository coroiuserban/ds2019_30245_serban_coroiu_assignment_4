package SD.assignment1.model;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

@Entity
public class MonitoredData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long patientId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String activity;
    private boolean valid;

    public MonitoredData(){
        activity = "init";
    }

    public MonitoredData(Long patientId, Long startTime, Long endTime, String activity, boolean valid){
        this.patientId = patientId;
        this.startTime = LocalDateTime.ofEpochSecond(startTime, 0, ZoneOffset.UTC);
        this.endTime = LocalDateTime.ofEpochSecond(endTime, 0, ZoneOffset.UTC);
        this.activity = activity;
        this.valid = valid;
    }

    public MonitoredData(Long id, Long patientId, Long startTime, Long endTime, String activity){
        this.id = id;
        this.patientId = patientId;
        this.startTime = LocalDateTime.ofEpochSecond(startTime, 0, ZoneOffset.UTC);
        this.endTime = LocalDateTime.ofEpochSecond(endTime, 0, ZoneOffset.UTC);
        this.activity = activity;
    }

    public LocalDateTime getStartTime(){
        return startTime;
    }

    public LocalDateTime getEndTime(){
        return endTime;
    }

    public String getActivity(){
        return activity;
    }

    public void setStartTime(LocalDateTime startTime){
        this.startTime = startTime;
    }

    public void setEndTime(LocalDateTime endTime){
        this.endTime = endTime;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public String toString(){
        return "id: " + patientId + ", start: " + startTime + ", end: "+ endTime+ ", activity: "+activity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}

