package SD.assignment1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /// Every medication has a period interval to take.
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @JoinTable(name = "medication", joinColumns = @JoinColumn(name = "medication_plan_id"), inverseJoinColumns = @JoinColumn(name = "medication_id"))
    private List<Medication> medicationPlan = new ArrayList<>();

    @ElementCollection(fetch=FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    private List<String> intakeIntervals;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    private int period;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private Patient patient;

    private LocalDate startTime;

    public MedicationPlan(List<Medication> medicationPlan, List<String> intakeIntervals, int period, Doctor doctor, LocalDate startTime) {
        this.intakeIntervals = intakeIntervals;
        this.medicationPlan = medicationPlan;
        this.period = period;
        this.doctor = doctor;
        this.startTime = startTime;
    }

    public MedicationPlan(List<Medication> medicationPlan, int period, Doctor doctor) {
        this.medicationPlan = medicationPlan;
        this.period = period;
        this.doctor = doctor;
    }

    public MedicationPlan(List<Medication> medicationPlan, int period) {
        this.medicationPlan = medicationPlan;
        this.period = period;
        this.doctor = new Doctor();
    }

    public MedicationPlan() {}

    public Long getId() {

        return id;
    }

    public List<String> getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(List<String> intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Medication> getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(List<Medication> medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public LocalDate getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDate startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString() {
        return "MedicationPlan{" +
                "id=" + id +
                ", medicationPlan=" + medicationPlan +
                ", period=" + period +
                '}';
    }
}
