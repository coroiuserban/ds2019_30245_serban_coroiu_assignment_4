package SD.assignment1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;

@Entity
public class Doctor extends User {

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "doctor_id")
    private List<MedicationPlan> medicationPlan;

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "doctor_id")
    private List<Patient> patient;


    public Doctor(String username, String password, String name, GENDER gender, String address, Date birthDate, List<MedicationPlan> medicationPlan, List<Patient> patient) {
        super(username, password, name, USERTYPE.Doctor, gender, address, birthDate);
        this.medicationPlan = medicationPlan;
        this.patient = patient;
    }

    public Doctor(String username, String password, String name, GENDER gender, String address, Date birthDate) {
        super(username, password, name, USERTYPE.Doctor, gender, address, birthDate);
        this.medicationPlan = new ArrayList<>();
    }

    public Doctor() {
    }

    public List<MedicationPlan> getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(List<MedicationPlan> medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public List<Patient> getPatient() {
        return patient;
    }

    public void setPatient(List<Patient> patient) {
        this.patient = patient;
    }
}
