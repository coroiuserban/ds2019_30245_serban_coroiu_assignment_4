package SD.assignment1.model;

import java.time.LocalDateTime;

public class MedicationStatusLogDTO {
    private long id;
    private String message;
    private long patientId;
    private String date;

    public MedicationStatusLogDTO(long id, String message, long patientId, String date) {
        this.id = id;
        this.message = message;
        this.patientId = patientId;
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }
}
