package SD.assignment1.model;

import org.springframework.stereotype.Component;

@Component
public class MonitoredDataDTO {

    private Long patientId;
    private Long startTime;
    private Long endTime;
    private String activity;

    public MonitoredDataDTO(){
        activity = "init";
    }

    public MonitoredDataDTO(Long patientId, Long startTime, Long endTime, String activity){
        this.patientId = patientId;
//        this.startTime = Instant.ofEpochMilli(startTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
//        this.endTime = Instant.ofEpochMilli(endTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
        this.startTime = startTime;
        this.endTime = endTime;
        this.activity = activity;
    }

    public Long getStartTime(){
        return startTime;
    }

    public Long getEndTime(){
        return endTime;
    }

    public String getActivity(){
        return activity;
    }

    public void setStartTime(Long startTime){
        this.startTime = startTime;
    }

    public void setEndTime(Long endTime){
        this.endTime = endTime;
    }

    public void setActivity(String activity){
        this.activity = activity;
    }

    public String toString(){
        return "start: " + startTime + ", end: "+ endTime+ ", activity: "+activity;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }
}


