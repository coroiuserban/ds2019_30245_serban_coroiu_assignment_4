package SD.assignment1.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class MedicationStatusLog {

    @Id
    @GeneratedValue
    private long id;
    private String message;
    private long patientId;
    private LocalDateTime date;

    public MedicationStatusLog(String message, long patientId, LocalDateTime date) {
        this.message = message;
        this.patientId = patientId;
        this.date = date;
    }

    public MedicationStatusLog(long id, String message, long patientId, LocalDateTime date) {
        this.id = id;
        this.message = message;
        this.patientId = patientId;
        this.date = date;
    }

    public MedicationStatusLog() { }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public LocalDateTime getDate() { return date; }

    public void setDate(LocalDateTime date) { this.date = date; }
}
