package SD.assignment1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * pacientii unui doctor : doctor/1/patients
 * login doctor: CRUD - patient/ caregiver/medicationPlan
 * login caregiver: see patient
 * login patient: see medication
 * security frontend
 */
@Entity
public class Patient extends User{

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;

    @OneToMany
    @JoinColumn(name = "patient_id")
    private List<MedicationPlan> medicationPlan = new ArrayList<>();
    private String medicalRecord;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    public Patient(String username, String password, String name, GENDER gender, String address, Date birthDate, Caregiver caregiver, List<MedicationPlan> medicationPlans, String medicalRecord, Doctor doctor){
        super(username, password, name, USERTYPE.Patient, gender, address, birthDate);
        this.caregiver = caregiver;
        this.medicationPlan = medicationPlans;
        this.medicalRecord = medicalRecord;
        this.doctor = doctor;
    }

    public Patient(String username, String password, String name, GENDER gender, String address, Date birthDate, String medicalRecord){
        super(username, password, name, USERTYPE.Patient, gender, address, birthDate);
        this.medicalRecord = medicalRecord;

    }

    public Patient() {

    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public List<MedicationPlan> getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(List<MedicationPlan> medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "caregiver=" + caregiver +
//                ", patientMedicationPlans=" + medicationPlan +
                super.toString() +
                '}';
    }
}
