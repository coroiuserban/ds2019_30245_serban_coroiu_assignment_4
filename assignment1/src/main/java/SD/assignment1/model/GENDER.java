package SD.assignment1.model;

import java.io.Serializable;

public enum GENDER implements Serializable {
    Male, Female
}
