package SD.assignment1.model;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Caregiver extends User {

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "caregiver_id")
    private List<Patient> patients = new ArrayList<>();

    public Caregiver(String username, String password, String name, GENDER gender, String address, Date birthDate, List<Patient> patients) {
        super(username, password, name, USERTYPE.Caregiver, gender, address, birthDate);
        this.patients = patients;
    }

    public Caregiver(String username, String password, String name, GENDER gender, String address, Date birthDate) {
        super(username, password, name, USERTYPE.Caregiver, gender, address, birthDate);
        this.patients = new ArrayList<>();
    }

    public Caregiver() {
    }

    public List<Patient> getPatients() {
        return patients;
    }

    public void setPatients(List<Patient> patients) {
        this.patients = patients;
    }

    @Override
    public String toString() {
        return "Caregiver{" +
                "patients=" + patients.size() +
                "user={ " + super.toString() +
                "}}";
    }
}
