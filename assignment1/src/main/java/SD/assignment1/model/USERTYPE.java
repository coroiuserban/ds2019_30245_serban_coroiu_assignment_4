package SD.assignment1.model;

import java.io.Serializable;

public enum USERTYPE implements Serializable {
    Doctor, Patient, Caregiver
}
