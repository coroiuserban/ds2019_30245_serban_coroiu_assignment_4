package SD.assignment1.filter;

import SD.assignment1.model.USERTYPE;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class DoctorFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
//        System.out.println("the header is: " + req.getHeader("authorization"));
        if(req.getMethod().equals("OPTIONS")) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
//        System.out.println("afteer");
        try {
            USERTYPE usertype = USERTYPE.valueOf(req.getHeader("authorization"));
            if(usertype == USERTYPE.Doctor) {
                filterChain.doFilter(servletRequest, servletResponse);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
