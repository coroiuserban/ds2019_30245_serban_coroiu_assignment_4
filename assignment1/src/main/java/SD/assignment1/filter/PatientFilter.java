package SD.assignment1.filter;

import SD.assignment1.model.USERTYPE;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class PatientFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        USERTYPE userRequest = USERTYPE.valueOf(req.getHeader("authorization"));
        if(userRequest == USERTYPE.Patient) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }
}
