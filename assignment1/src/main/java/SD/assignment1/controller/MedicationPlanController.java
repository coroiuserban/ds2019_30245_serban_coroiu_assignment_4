package SD.assignment1.controller;

import SD.assignment1.model.MedicationPlan;
import SD.assignment1.service.MedicationPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/medicationPlan")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MedicationPlanController {

    @Autowired
    private MedicationPlanService medicationPlanService;

    @RequestMapping(value = "/{doctorId}/{patientId}/create", method = RequestMethod.POST)
    public void createMedicationPlan(@PathVariable("doctorId")Long doctorId, @PathVariable("patientId")Long patientId, @RequestBody MedicationPlan medicationPlan) {
        medicationPlanService.createMedicationPlan(patientId, doctorId, medicationPlan);
    }
}