package SD.assignment1.controller;

import SD.assignment1.model.Doctor;
import SD.assignment1.model.Medication;
import SD.assignment1.model.MedicationPlan;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.DoctorRepository;
import SD.assignment1.repository.MedicationRepository;
import SD.assignment1.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/doctor")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @RequestMapping(value = "/{id}/patients", method = RequestMethod.GET)
    public List<Patient> getPatients(@PathVariable Long id) {
        return doctorService.getPatientsForDoctor(id);
    }

    @RequestMapping(value = "/{id}/medicationPlans", method = RequestMethod.GET)
    public List<MedicationPlan> getMedicationPlans(@PathVariable Long id) {
        return doctorService.getMedicationPlansCreatedBy(id);
    }

    @RequestMapping(value = "/medications", method = RequestMethod.GET)
    public List<Medication> getMedicatios() {
        return doctorService.getAvailableMedications();
    }
//
//    @RequestMapping(value = "/create", method = RequestMethod.POST)
//    public void createPatient(@RequestBody Patient patient) {
//        patient.setCaregiver(null);
//        patient.setMedicationPlan(null);
//        patientRepository.save(patient);
//        }
//
//    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
//    public void deletePatient( @PathVariable("id") Long id){
//        patientRepository.deleteById(id);
//    }
}
