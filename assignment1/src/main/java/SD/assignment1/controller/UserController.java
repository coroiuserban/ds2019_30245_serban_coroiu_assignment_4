package SD.assignment1.controller;

import SD.assignment1.model.User;
import SD.assignment1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/user")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public User getUser(@RequestBody Long id){
        return userRepository.findUserById(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<User> getUsers(){
        return userRepository.findAll();
    }
}
