package SD.assignment1.controller;

import SD.assignment1.model.*;
import SD.assignment1.repository.*;

import SD.assignment1.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.print.Doc;
import java.util.List;

@RequestMapping("/patient")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class PatientController {

    @Autowired
    private PatientService patientService;


    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Patient getPatient(@RequestBody Long id) {
        return patientService.getPatientWith(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Patient> getPatients() {
        return patientService.getAllPatients();
    }

    @RequestMapping(value = "/{doctorId}/create", method = RequestMethod.POST)
    public void createPatient(@PathVariable("doctorId")Long doctorId, @RequestBody Patient patient) {
        patientService.createPatient(doctorId, patient);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    public void deletePatient( @PathVariable("id") Long id){
        patientService.deletePatient(id);
    }

    @RequestMapping(value = "/{patientId}/assign", method = RequestMethod.POST)
    public void assignPatientToCaregiver(@PathVariable("patientId")Long patientId, @RequestBody Long caregiverId){
        patientService.assignPatientToCaregiver(patientId, caregiverId);
    }

    @RequestMapping(value = "/createMedication", method = RequestMethod.POST)
    public void createMedication(@RequestBody Medication medication) {
        patientService.createMedication(medication);
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST)
    public void updatePatient(@PathVariable("id")Long id, @RequestBody Patient patient) {
        patientService.updatePatient(id, patient);
    }

    @RequestMapping(value = "/{id}/caregiver", method = RequestMethod.GET)
    public Caregiver getCaregiverForPatient(@PathVariable("id")Long id) {
        return patientService.getCaregiverForPatient(id);
    }
}