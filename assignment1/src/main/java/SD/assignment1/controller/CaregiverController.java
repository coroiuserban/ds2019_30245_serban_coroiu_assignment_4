package SD.assignment1.controller;

import SD.assignment1.model.Caregiver;
import SD.assignment1.model.Patient;
import SD.assignment1.service.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequestMapping("/caregiver")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CaregiverController {

    @Autowired
    private CaregiverService caregiverService;
    /***
     *
     * @param id
     * @return caregiver with @id
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Caregiver getCaregiver(@RequestBody Long id) {
       return caregiverService.getCaregiverWith(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Caregiver> getCaregivers() {
       return caregiverService.getAllCaregivers();
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createCaregiver(@RequestBody Caregiver caregiver) {
        caregiverService.createCaregiver(caregiver);
    }

    @RequestMapping(value = "/{id}/delete", method = RequestMethod.DELETE)
    public void deleteCaregiver( @PathVariable("id") Long id){
        caregiverService.deleteCaregiverWith(id);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateCaregiver(@RequestBody Caregiver caregiver) {
        caregiverService.updateCaregiver(caregiver);
    }

    @RequestMapping(value = "/{id}/patients", method = RequestMethod.GET)
    public List<Patient> getPatientsForCaregiver(@PathVariable("id")Long id) {
        return caregiverService.getPatientsForCaregiverWith(id);
    }

}
