package SD.assignment1.controller;

import SD.assignment1.model.Doctor;
import SD.assignment1.model.USERTYPE;
import SD.assignment1.model.User;
import SD.assignment1.repository.DoctorRepository;
import SD.assignment1.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/login")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;
    @Autowired
    private DoctorRepository doctorRepository;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public User verifyCredentials(String username, String password) {
        System.out.println("Request received" + username + " " + password);
        return loginService.loginUser(username, password);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public void createDoctor(@RequestBody Doctor doctor) {
        System.out.println(doctor);
        doctor.setMedicationPlan(null);
        doctor.setPatient(null);
        doctorRepository.save(doctor);
    }
}
