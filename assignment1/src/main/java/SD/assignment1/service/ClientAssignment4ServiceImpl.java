package SD.assignment1.service;

import SD.assignment1.model.*;
import SD.assignment1.repository.CaregiverRepository;
import SD.assignment1.repository.MedicationStatusLogRepository;
import SD.assignment1.repository.MonitoredDataRepository;
import SD.assignment1.repository.RecomandationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@WebService
public class ClientAssignment4ServiceImpl implements ClientAssignment4Service {

    @Autowired
    MonitoredDataRepository monitoredDataRepository;
    @Autowired
    MedicationStatusLogRepository medicationStatusLogRepository;
    @Autowired
    RecomandationRepository recomandationRepository;

    public ClientAssignment4ServiceImpl() {
        System.out.println("created");
    }

    @Override
    @WebMethod(operationName = "getActivitiesForPatient")
    public List<PatientActivity> getActivitiesForPatient(@WebParam(name = "patientId") Long patientId) {
        List<MonitoredData> monitoredData = monitoredDataRepository.findAllByPatientId(patientId);
        if(monitoredData.isEmpty()) {
            return new ArrayList<>();
        }
        List<PatientActivity> activities = new ArrayList<>();
        activities = monitoredData.stream().map(data -> {
            Long interval = data.getStartTime().until(data.getEndTime(), ChronoUnit.MINUTES);
            return new PatientActivity(data.getId(), interval, data.getActivity(), data.getStartTime().toString(), data.isValid());
        }).collect(Collectors.toList());
        return activities;
    }

    @Override
    public List<MedicationStatusLogDTO> getMedicationStatusForPatient(Long patientId) {
        List<MedicationStatusLog> medicationStatusLogs = medicationStatusLogRepository.findAllByPatientId(patientId);
        if(!medicationStatusLogs.isEmpty()) {
            return medicationStatusLogs.stream().map(data -> new MedicationStatusLogDTO(data.getId(), data.getMessage(), data.getPatientId(), data.getDate().toString())).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public void markActivityAsNormal(Long activityId) {
        MonitoredData activity = monitoredDataRepository.findById(activityId).get();
        activity.setValid(true);
        monitoredDataRepository.save(activity);
    }

    @Override
    public void addRecomandation(Recomandation recomandation) {
        recomandationRepository.save(recomandation);
    }

    @Override
    public List<Recomandation> getRecomandations(Long forPatientId) {
        System.out.println(recomandationRepository.findAllByPatientId(forPatientId));
        return recomandationRepository.findAllByPatientId(forPatientId);
    }


}

