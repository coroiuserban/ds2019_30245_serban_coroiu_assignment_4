package SD.assignment1.service;

import SD.assignment1.model.Caregiver;
import SD.assignment1.model.MonitoredData;
import SD.assignment1.model.MonitoredDataDTO;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.MonitoredDataRepository;
import SD.assignment1.repository.PatientRepository;
import SD.assignment1.service.PatientService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

@Component
@EnableScheduling
public class Consumer {

    FileWriter writer = null;

    @Autowired
    private MonitoredDataRepository monitoredDataRepository;
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
    @Autowired
    private PatientService patientService;

    public Consumer() {

        try {
            writer = new FileWriter("activity.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<Long> patients = Arrays.asList(9L, 19L, 22L, 24L, 33L, 35L);

    @RabbitListener(queues="${jsa.rabbitmq.queue}", containerFactory="jsaFactory")
    public void recievedMessage(MonitoredDataDTO msg) {
        MonitoredData monitoredData = new MonitoredData(msg.getPatientId(), msg.getStartTime(), msg.getEndTime(), msg.getActivity(), true);
        int rand = (int) (Math.random()*(patients.size()));
        monitoredData.setPatientId(patients.get(rand));
        long between = ChronoUnit.HOURS.between(monitoredData.getStartTime(), monitoredData.getEndTime());
        if(msg.getActivity().contains("Sleeping") && between > 12) {
            System.out.println("WRONG: " + monitoredData);
            monitoredData.setValid(false);
            sendMessage("Patient with id: " + monitoredData.getPatientId() + " is sleeping for more than 12 hours.", monitoredData.getPatientId());
        }
        if(msg.getActivity().contains("Leaving") && between > 12) {
            System.out.println("WRONG: " + monitoredData);
            sendMessage("Patient with id: " + monitoredData.getPatientId() + " is outside for more than 12 hours.", monitoredData.getPatientId());
            monitoredData.setValid(false);
        }
        if((msg.getActivity().contains("Toileting") || msg.getActivity().contains("Showering")) && between > 1)  {
            System.out.println("WRONG: " + monitoredData);
            sendMessage("Patient with id: " + monitoredData.getPatientId() + " is in bathroom for more than 1 hour.", monitoredData.getPatientId());
            monitoredData.setValid(false);
        }
        monitoredDataRepository.save(monitoredData);
//        try {
//            int rand = (int) (Math.random()*(patients.size()));
//            writer.write(rand +","+ monitoredData.getStartTime() +","+ monitoredData.getEndTime() +","+ monitoredData.getActivity() + "\n");
//            writer.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        System.out.println("Recieved Message: " + monitoredData);
    }

    private void sendMessage(String msg, Long patientId) {
        Caregiver caregiver = patientService.getCaregiverForPatient(patientId);
        System.out.println("sending... + " + msg + "to: " + caregiver.getId());
        messagingTemplate.convertAndSend("/notification/" + caregiver.getId(), msg);
    }

//    @Scheduled(fixedDelay = 1000)
//    private void sendMessage() {
//        messagingTemplate.convertAndSend("/notification/" + 25, "jasjdakjda");
//    }
}
