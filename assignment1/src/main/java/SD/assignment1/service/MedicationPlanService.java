package SD.assignment1.service;

import SD.assignment1.model.Doctor;
import SD.assignment1.model.MedicationPlan;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.DoctorRepository;
import SD.assignment1.repository.MedicationPlanRepository;
import SD.assignment1.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicationPlanService {
    @Autowired
    private MedicationPlanRepository medicationPlanRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private PatientRepository patientRepository;

    public void createMedicationPlan(Long patientId, Long doctorId, MedicationPlan medicationPlan) {
        Doctor doctor = doctorRepository.findById(doctorId).get();
        medicationPlan.setDoctor(doctor);
        Patient patient = patientRepository.findPatientById(patientId);
        medicationPlan.setPatient(patient);
        medicationPlanRepository.save(medicationPlan);
    }
}
