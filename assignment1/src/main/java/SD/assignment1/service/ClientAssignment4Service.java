package SD.assignment1.service;

import SD.assignment1.model.*;
import org.springframework.stereotype.Service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Service
public interface ClientAssignment4Service {

    @WebMethod
    List<PatientActivity> getActivitiesForPatient(Long patientId);

    @WebMethod
    List<MedicationStatusLogDTO> getMedicationStatusForPatient(Long patientId);

    @WebMethod
    void markActivityAsNormal(Long activityId);

    @WebMethod
    void addRecomandation(Recomandation recomandation);

    @WebMethod
    List<Recomandation> getRecomandations(Long forPatientId);
}
//wsimport -keep -p service -d D:\Faculta\IV\ass3\ds2019_30245_serban_coroiu_assignment_3\assignment3\src\main\java\com\sd\assignment3 http://localhost:2750/employeeservice?wsdl
