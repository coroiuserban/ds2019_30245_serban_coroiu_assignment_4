package SD.assignment1.service;

import SD.assignment1.model.Caregiver;
import SD.assignment1.model.Doctor;
import SD.assignment1.model.Medication;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.CaregiverRepository;
import SD.assignment1.repository.DoctorRepository;
import SD.assignment1.repository.MedicationRepository;
import SD.assignment1.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private MedicationRepository medicationRepository;
    @Autowired
    private CaregiverRepository caregiverRepository;

    public Patient getPatientWith(Long id) {
        return patientRepository.findPatientById(id);
    }

    public List<Patient> getAllPatients() {
        return patientRepository.findAll();
    }

    public void createPatient(Long doctorId, Patient patient) {
        patient.setCaregiver(null);
        patient.setMedicationPlan(null);
        Doctor doctor = doctorRepository.findById(doctorId).get();
        patient.setDoctor(doctor);
        patientRepository.save(patient);
    }

    public void deletePatient(Long id) {
        patientRepository.deleteById(id);
    }

    public void assignPatientToCaregiver(Long patientId, Long caregiverId) {
        Patient patient = patientRepository.findPatientById(patientId);
        Caregiver caregiver = caregiverRepository.findById(caregiverId).get();
        patient.setCaregiver(caregiver);
        patientRepository.save(patient);
    }

    public void createMedication(Medication medication) {
        medication.setMedicationPlans(null);
        medicationRepository.save(medication);
    }

    public void updatePatient(Long caregiverId, Patient patient) {
        if (caregiverId != -1) {
            Caregiver caregiver = caregiverRepository.findById(caregiverId).get();
            if(!caregiver.getPatients().contains(patient)) {
                patient.setCaregiver(caregiver);
            }
        } else {
            Patient existingP = patientRepository.findPatientById(patient.getId());
            patient.setCaregiver(existingP.getCaregiver());
        }
        patientRepository.save(patient);
    }

    public Caregiver getCaregiverForPatient(Long id) {
        return patientRepository.findPatientById(id).getCaregiver();
    }
}
