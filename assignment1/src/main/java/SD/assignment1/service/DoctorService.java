package SD.assignment1.service;

import SD.assignment1.model.Doctor;
import SD.assignment1.model.Medication;
import SD.assignment1.model.MedicationPlan;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.DoctorRepository;
import SD.assignment1.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DoctorService {
    @Autowired
    private DoctorRepository doctorRepository;
    @Autowired
    private MedicationRepository medicationRepository;

    public List<Patient> getPatientsForDoctor(Long id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(doctor.isPresent()) {
            return doctor.get().getPatient();
        } else {
            return new ArrayList<>();
        }
    }

    public List<MedicationPlan> getMedicationPlansCreatedBy(Long id) {
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(doctor.isPresent()) {
            return doctor.get().getMedicationPlan();
        } else {
            return new ArrayList<>();
        }
    }

    public List<Medication> getAvailableMedications() {
        return medicationRepository.findAllMedications();
    }
}
