package SD.assignment1.service;

import SD.assignment1.model.Patient;
import SD.assignment1.model.USERTYPE;
import SD.assignment1.model.User;
import SD.assignment1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UserRepository userRepository;

    public User loginUser(String username, String password) {
        System.out.println("inside login service");
        User user = userRepository.findUserByUsernameAndPassword(username, password);
        if(user != null){
            return user;
        } else {
            System.out.println("Username/ passwprd incorrect");
            return null;
        }

    }
}
