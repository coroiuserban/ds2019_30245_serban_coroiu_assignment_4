package SD.assignment1.service;

import SD.assignment1.model.MedicationPlan;

import java.util.List;

public interface ClientAssignment3Service {
    String getString(String pickUpLocation);
    long canLogin(String withUsername, String withPassword);
    List<MedicationPlan> getMedications(long forClientWithId);
    void medicationLog(String log, long patientId);
}
