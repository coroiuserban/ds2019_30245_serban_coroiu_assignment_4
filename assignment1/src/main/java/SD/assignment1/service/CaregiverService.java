package SD.assignment1.service;


import SD.assignment1.model.Caregiver;
import SD.assignment1.model.Patient;
import SD.assignment1.repository.CaregiverRepository;
import SD.assignment1.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {

    @Autowired
    private CaregiverRepository caregiverRepository;
    @Autowired
    private PatientRepository patientRepository;

    public Caregiver getCaregiverWith(Long id) {
        Optional<Caregiver> caregiver =  caregiverRepository.findById(id);
        return caregiver.orElse(null);
    }

    public List<Caregiver> getAllCaregivers() {
        return caregiverRepository.findAll();
    }

    public void createCaregiver(Caregiver caregiver) {
        caregiver.setPatients(null);
        caregiverRepository.save(caregiver);
    }

    public void deleteCaregiverWith(Long id) {
        caregiverRepository.deleteById(id);
    }

    public void updateCaregiver(Caregiver caregiver){
        caregiverRepository.save(caregiver);
    }

    public List<Patient> getPatientsForCaregiverWith(Long id) {
        return patientRepository.findPatientsByCaregiverId(id);
    }
}
