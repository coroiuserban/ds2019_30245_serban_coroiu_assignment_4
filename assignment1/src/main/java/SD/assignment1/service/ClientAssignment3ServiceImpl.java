package SD.assignment1.service;

import SD.assignment1.model.MedicationPlan;
import SD.assignment1.model.MedicationStatusLog;
import SD.assignment1.model.USERTYPE;
import SD.assignment1.model.User;
import SD.assignment1.repository.MedicationPlanRepository;
import SD.assignment1.repository.MedicationStatusLogRepository;
import SD.assignment1.repository.PatientRepository;
import SD.assignment1.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientAssignment3ServiceImpl implements ClientAssignment3Service {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MedicationPlanRepository medicationPlanRepository;
    @Autowired
    private MedicationStatusLogRepository medicationStatusLogRepository;

    @Override
    public String getString(String pickUpLocation) {
        return pickUpLocation + " from server";
    }

    @Override
    public long canLogin(String withUsername, String withPassword) {
        User user = userRepository.findUserByUsernameAndPassword(withUsername, withPassword);
        if(user != null && user.getRole() == USERTYPE.Patient) { return user.getId(); }
        return -1;
    }

    @Override
    public List<MedicationPlan> getMedications(long forClientWithId) {
//        System.out.println(forClientWithId);
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findMedicationPlanByPatientId(forClientWithId).stream().filter(element -> element.getStartTime().plusDays(element.getPeriod()).isAfter(LocalDate.now())).collect(Collectors.toList());
//        System.out.println(medicationPlans);
        return medicationPlans;
    }

    @Override
    public void medicationLog(String log, long patientId) {
        System.out.println("log received... : ");
        System.out.println(log + " for: " + patientId);
        MedicationStatusLog messageLog = new MedicationStatusLog(log, patientId, LocalDateTime.now());
        medicationStatusLogRepository.save(messageLog);
    }
}
