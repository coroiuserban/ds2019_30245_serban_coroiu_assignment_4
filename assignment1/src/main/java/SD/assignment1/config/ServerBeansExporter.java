package SD.assignment1.config;

import SD.assignment1.service.ClientAssignment3Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
public class ServerBeansExporter {

    @Bean
    RmiServiceExporter exporter(ClientAssignment3Service implementation) {
        Class<ClientAssignment3Service> serviceInterface = ClientAssignment3Service.class;
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceInterface(serviceInterface);
        exporter.setService(implementation);
        exporter.setServiceName(serviceInterface.getSimpleName());
        exporter.setRegistryPort(1099);
        return exporter;
    }
}
