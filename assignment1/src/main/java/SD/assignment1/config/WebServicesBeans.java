package SD.assignment1.config;

import SD.assignment1.service.ClientAssignment4ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

import javax.xml.ws.Endpoint;

@Configuration
public class WebServicesBeans {

    @Autowired
    private ClientAssignment4ServiceImpl clientAssignment4Service;

    @Bean
    public void setupClientAssignment3Service() {
        System.out.println("exposing Service for Assignment4");
        System.out.println(clientAssignment4Service);
        Endpoint.publish("http://localhost:2750/employeeservice",
                clientAssignment4Service);
    }
}
