package SD.assignment1.config;

import SD.assignment1.service.ClientAssignment3Service;
import SD.assignment1.service.ClientAssignment3ServiceImpl;
import org.springframework.context.annotation.Bean;

public class ConfigureBeans {

    @Bean
    ClientAssignment3Service getString() {
        return new ClientAssignment3ServiceImpl();
    }
}
