package SD.assignment1;

import SD.assignment1.filter.CaregiverFilter;
import SD.assignment1.filter.DoctorFilter;
import SD.assignment1.filter.PatientFilter;
import SD.assignment1.service.ClientAssignment4ServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

import javax.xml.ws.Endpoint;

@SpringBootApplication
public class Assignment1Application {

	public static void main(String[] args) {
		SpringApplication.run(Assignment1Application.class, args);
	}

	@Configuration
	@EnableWebSocketMessageBroker
	static class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {
		@Override
		public void registerStompEndpoints(StompEndpointRegistry registry) {
			registry.addEndpoint("/socket").setAllowedOrigins("http://localhost:4200").withSockJS();
		}

		@Override
		public void configureMessageBroker(MessageBrokerRegistry registry) {
			registry.enableSimpleBroker("/notification"); // ma conectez la el de pe UI
            registry.setApplicationDestinationPrefixes("/app");
		}
	}

//	@Bean
//	public FilterRegistrationBean<DoctorFilter> doctorFilterRegistration(){
//		FilterRegistrationBean<DoctorFilter> registrationBean
//				= new FilterRegistrationBean<>();
//		registrationBean.setFilter(new DoctorFilter());
//		registrationBean.addUrlPatterns("/doctor/*");
//		registrationBean.addUrlPatterns("/patient/*");
//		registrationBean.addUrlPatterns("/caregiver/*");
//		registrationBean.addUrlPatterns("/medicationPlan/*");
//		return registrationBean;
//	}
//
//	@Bean
//	public FilterRegistrationBean<CaregiverFilter> caregiverFilterRegistration(){
//		FilterRegistrationBean<CaregiverFilter> registrationBean
//				= new FilterRegistrationBean<>();
//		registrationBean.setFilter(new CaregiverFilter());
////		registrationBean.addUrlPatterns("/login/*");
//		registrationBean.addUrlPatterns("/caregiver/*");
//		return registrationBean;
//	}
//
//	@Bean
//	public FilterRegistrationBean<PatientFilter> patientFilterRegistration(){
//		FilterRegistrationBean<PatientFilter> registrationBean
//				= new FilterRegistrationBean<>();
//		registrationBean.setFilter(new PatientFilter());
////		registrationBean.addUrlPatterns("/login/*");
//		registrationBean.addUrlPatterns("/patient/*");
//		return registrationBean;
//	}
//
//	@Autowired
//	private MedicationRepository medicationRepository;
//	@Autowired
//	private MedicationPlanRepository medicationPlanRepository;
//	@Autowired
//	private PatientRepository patientRepository;
//	@Autowired
//	private DoctorRepository doctorRepository;
//
//	@Autowired
//	private CaregiverRepository caregiverRepository;
//
//	@Bean
//	CommandLineRunner init(UserRepository userRepository) {
//		return args -> {
//
//			Doctor doctor = new Doctor("user2", "abcd", "Andrei Lucian", GENDER.Male, "Cluj-Napoca",Date.valueOf("1997-03-10"));
////			doctorRepository.save(doctor);
////
//			Caregiver user2 = new Caregiver("user1", "abcd", "Andrei Lucian", GENDER.Male, "Cluj-Napoca",Date.valueOf("1997-04-10"));
////			userRepository.save(user2);
//////
//			Medication med1 = new Medication("Nurofen", "dureri de burta", 500);
//			Medication med2 = new Medication("FaringoSept", "greata", 100);
////			medicationRepository.save(med1);
////			medicationRepository.save(med2);
//			List<Medication> medications = Arrays.asList(med1, med2);
////			System.out.println(patientRepository.findAll());
////			Patient patient = patientRepository.findPatientById(2L);
//			List<MedicationPlan> medicationPlans = new ArrayList<>();
//			MedicationPlan plan1 = new MedicationPlan(medications, 14);
////			medicationPlanRepository.save(plan1);
//			medicationPlans.add(plan1);
//			Patient user1 = new Patient("user1", "abcd", "Dorel", GENDER.Male, "Cluj-Napoca",Date.valueOf("1997-04-10"), "alergic la facultate");
//			user1.setMedicationPlan(medicationPlanRepository.findMedicationPlanById(2L));
//			user1.setCaregiver(caregiverRepository.findById(2L).get());
////			userRepository.save(user1);
////			MedicationPlan result = medicationPlanRepository.findById(1L).get();
////			System.out.println(result.getMedicationPlan()); //works. It fetch all the med that are contained in medicationPlan
//
//		};
//	}

}
