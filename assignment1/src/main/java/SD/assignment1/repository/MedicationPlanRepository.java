package SD.assignment1.repository;

import SD.assignment1.model.Medication;
import SD.assignment1.model.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Long> {
    List<MedicationPlan> findMedicationPlanByPatientId(Long id);
    List<MedicationPlan> findMedicationPlanById(Long id);
}
