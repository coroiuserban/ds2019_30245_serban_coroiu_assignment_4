package SD.assignment1.repository;

import SD.assignment1.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Long> {

    @Query(value = "SELECT u FROM Medication u WHERE dosage > 0")
    List<Medication> findAllMedications();
}
