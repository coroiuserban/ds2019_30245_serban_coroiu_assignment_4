package SD.assignment1.repository;

import SD.assignment1.model.Recomandation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecomandationRepository extends JpaRepository<Recomandation, Long> {
    List<Recomandation> findAllByPatientId(Long patientId);
}
