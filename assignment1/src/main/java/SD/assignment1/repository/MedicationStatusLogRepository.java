package SD.assignment1.repository;

import SD.assignment1.model.MedicationStatusLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationStatusLogRepository extends JpaRepository<MedicationStatusLog, Long> {
    List<MedicationStatusLog> findAllByPatientId(Long patientId);
}
