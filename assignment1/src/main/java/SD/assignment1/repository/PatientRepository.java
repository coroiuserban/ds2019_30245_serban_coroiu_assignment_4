package SD.assignment1.repository;

import SD.assignment1.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    Patient findPatientById(Long id);
    List<Patient> findPatientsByCaregiverId(Long id);
}
