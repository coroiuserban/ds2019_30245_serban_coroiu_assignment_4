package SD.assignment1.repository;

import SD.assignment1.model.MonitoredData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MonitoredDataRepository extends JpaRepository<MonitoredData, Long> {
    List<MonitoredData> findAllByPatientId(Long patientId);
}
