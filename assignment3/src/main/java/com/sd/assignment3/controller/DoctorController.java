package com.sd.assignment3.controller;

import com.sd.assignment3.model.MedicationStatusLog;
import com.sd.assignment3.model.RecomandationDTO;
import com.sd.assignment3.service.*;
import org.springframework.web.bind.annotation.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/assignment4")
@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class DoctorController {

    private ClientAssignment4ServiceImpl service;

    public DoctorController() {
        URL url = null;
        try {
            url = new URL("http://localhost:2750/employeeservice?wsdl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ClientAssignment4ServiceImplService clientService = new ClientAssignment4ServiceImplService(url);
        service = clientService.getClientAssignment4ServiceImplPort();
    }

    @RequestMapping(value = "/activities/{patientId}", method = RequestMethod.POST)
    public List<PatientActivity> getAllActivitiesFor(@PathVariable Long patientId, @RequestBody LocalDate forDate) {
//        System.out.println(service.getActivitiesForPatient(id).get(0).getStartTime());
        return service.getActivitiesForPatient(patientId).stream()
                .filter(elem -> LocalDateTime.parse(elem.getStartTime()).toLocalDate().isEqual(forDate))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/medicationLog/{patientId}", method = RequestMethod.POST)
    public List<MedicationStatusLog> getMedicationLogFor(@PathVariable Long patientId, @RequestBody LocalDate forDate) {
//        System.out.println(patientId + " " + forDate);
        List<MedicationStatusLogDTO> medicationStatusLogDTOS = service.getMedicationStatusForPatient(patientId);
        return medicationStatusLogDTOS.stream()
                .map( e -> new MedicationStatusLog(e.getId(), e.getMessage(), e.getPatientId(), LocalDateTime.parse(e.getDate())))
                .filter( elem -> elem.getDate().toLocalDate().isEqual(forDate))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/activities/valid", method = RequestMethod.PUT)
    public void markActivityAsNormal(@RequestBody Long activityId) {
        service.markActivityAsNormal(activityId);
//        System.out.println(activityId);
    }

    @RequestMapping(value = "/activities/recomandation", method = RequestMethod.POST)
    public void addRecomandation(@RequestBody Recomandation recomandation) {
        service.addRecomandation(recomandation);
//        System.out.println(recomandation.getActivityId() + " " + recomandation.getMessage());
    }

    @RequestMapping(value = "/activities/recomandation/{patientId}", method = RequestMethod.GET)
    public List<Recomandation> getRecomandations(@PathVariable Long patientId) {
        return service.getRecomandations(patientId);
    }
}
